# -*- coding: utf-8 -*-
# This file is part of health_digital_prescription_fiuner module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

from trytond.pool import Pool
from . import health_stock
from .report import *
from .wizard import *


def register():
    Pool.register(
        health_stock.PatientPrescriptionOrder,
        health_stock.PrescriptionLine,
        health_stock.DeliverPrescriptionLineStart,
        create_medicament_category_movement_report.CreateMedicamentCategoryMovementReportStart,
        module='health_digital_prescription_fiuner', type_='model')
    Pool.register(
        health_stock.DeliverPrescriptionLine,
        create_medicament_category_movement_report.CreateMedicamentCategoryMovementReportWizard,
        module='health_digital_prescription_fiuner', type_='wizard')
    Pool.register(
        medicament_category_movement_report.MedicamentCategoryMovementReport,
        module='health_digital_prescription_fiuner', type_='report')
