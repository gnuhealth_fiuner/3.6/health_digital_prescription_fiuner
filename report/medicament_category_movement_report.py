# -*- coding: utf-8 -*-

from trytond.pool import Pool
from trytond.report import Report
from trytond.transaction import Transaction

from datetime import datetime
from dateutil.relativedelta import relativedelta


__all__ = ['MedicamentCategoryMovementReport']

class MedicamentCategoryMovementReport(Report):
    'Program Remediar Report'
    __name__ = 'gnuhealth.medicament.category.movement_report'

    @classmethod
    def get_context(cls, records, data):
        context = super(MedicamentCategoryMovementReport,cls).get_context(records,data)

        pool = Pool()
        Location = pool.get('stock.location')
        Move = pool.get('stock.move')
        Medicament = pool.get('gnuhealth.medicament')
        MedicamentCategory = pool.get('gnuhealth.medicament.category')
        Product = pool.get('product.product')
        Template = pool.get('product.template')

        #set variables
        start = context['start'] = data['start']
        end = context ['end'] = data['end']
        location = data['location']
        category_selection = data['category_selection']
        categories = data['categories']
        medicament_category_none = data['medicament_category_none']

        medicament_categories = []
        if category_selection == 'default':
            medicament_categories = MedicamentCategory.search([('name','=','REMEDIAR')])
        else:
            medicament_categories = MedicamentCategory.search([('id','in',categories)])
        #all the medicaments that belong to the selected categories
        if medicament_category_none == True:
            medicaments = Medicament.search(
                ['OR',
                ('category.id','in',[x.id for x in medicament_categories]),
                ('category','=',None)
                ])
        else:
            medicaments = Medicament.search([('category.id','in',[x.id for x in medicament_categories])])
        #all the products that are related to the medicaments
        products = Product.search([('template.id','in',[x.name.template.id for x in medicaments])])
        #all the templates related to the products
        templates = Template.search([('id','in',[x.template.id for x in products])])
        #the warehouse
        locations = Location.search([('id','=',location)])
        #the quantity on that location
        products_by_location = Product.products_by_location(
                    location_ids = [x.id for x in locations],
                    with_childs=True)

        #all the output moves
        output_moves = Move.search([
            ('effective_date','>=',start),
            ('effective_date','<=',end),
            ('from_location','=',locations[0].storage_location.id),
            ('product','in',[x.id for x in products]),
            ])

        input_moves = Move.search([
            ('effective_date','>=',start),
            ('effective_date','<=',end),
            ('to_location','=',locations[0].storage_location.id),
            ('product','in',[x.id for x in products])
            ])

        context['medicaments'] = []

        #we set up our context based on the templates,not on the medicaments list above
        for template in templates:
            #incoming movements
            incoming = len([x for x in input_moves\
                if x.product.id in [y.id for y in products]\
                    and x.product.template.id == template.id
                ])
            #outcoming movements
            outcoming = len([x for x in output_moves\
                if x.product.id in [y.id for y in products]\
                    and x.product.template.id == template.id
                ])
            #quantities incoming
            qty_in = sum([x.quantity for x in input_moves\
                if x.product.id in [y.id for y in products]\
                    and x.product.template.id == template.id
                ])
            #quantities outcoming
            qty_out = sum([x.quantity for x in output_moves\
                if x.product.id in [y.id for y in products]\
                    and x.product.template.id == template.id
                ])
            #remaining quantities
            remaining = sum([products_by_location[(locations[0].id,x.id)] for x in products\
                if x.id in [y.id for y in template.products]])
            context['medicaments'].append({
                'name': template.name,
                'code': ', '.join([x.code or '' for x in products\
                    if x.id in [y.id for y in template.products] and x.code != '']),
                'incoming': incoming,
                'outcoming': outcoming,
                'qty_in': qty_in,
                'qty_out': qty_out,
                'initial_qty': remaining+qty_out,
                'remaining': remaining,
                })
        #context = sorted(context, key=lambda k: k['name'])
        return context
 
